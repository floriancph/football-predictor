# Digital Offensive Coordinator
American Football looks on the first glance often like a very chaotic sport - 22 men trying to have as much ball possession as possible. But if you take a deeper look into it, it shows that nothing happens randomly. All of the actions of each individual player are planned and belong to a tactical plan.

One of the major questions of each Offensive Coordinator is 'How do I maximize my chances to get a new First Down'? And 'Does it make sense to play the 4th attempt or would it be stupid and should I punt instead'? To find answers to these questions, it does make a lot of sense to learn from the existing data:
The NFL published a huge amount of data on Kaggle with information about each player for each play, the applied formations and the achieved yards after the snap.

This repository provides:
- An analysis of the data in a jupyter notebook and training of a classification model in order to predict the probability of a new first down for the offense
- A WebApp which makes use of the trained predition model and enables an interactive interface to play with the model.

## Landing Page of the Website
<img src="img/homepage.png">

## Prediction result per Offensive Formation
<img src="img/prediction_result.png">

## Similar plays
<img src="img/similar_play.png">

# Getting Started
Main language used is Python 3.9 with some simple HTML code for the WebApp. I also provide a Dockerfile, which makes it easy to start the WebApp.

## Python Jupyter Notebook
Make sure, that you have installed all the packages listed in the requirements.txt file. Otherwise perform
- ```pip install -r requirements.txt``` (you might want to do it in a virtual environment)

Now you should be able to execute the notebook in notebook/football.ipynb

## WebApp in a Docker Container
For the WebApp, I would recommend to create a Docker image using the Dockerfile provided:
- Build Container from Dockerfile locally: ```docker build --tag football .```
- Run container locally on port 5000: ```docker run -d -p 5000:5000 football```

Now you should be able to see the WebApp on localhost:5000

## Publish Docker Image into AWS Container Registry
- login via aws cli using: ```aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin xxx.dkr.ecr.eu-central-1.amazonaws.com```
- ```docker tag football:latest xxx.dkr.ecr.eu-central-1.amazonaws.com/football:latest```
- ```docker push xxx.dkr.ecr.eu-central-1.amazonaws.com/football:latest```
