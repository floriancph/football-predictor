from flask import Flask, url_for, request, redirect
from flask import render_template, Response
from sklearn.preprocessing import StandardScaler
import numpy as np
import pandas as pd 
import pickle as pkl
import os
import io
import random
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import matplotlib.patches as patches
import sys

app = Flask(__name__)

@app.route('/')
def index():
    err = request.args.get("err")
    return render_template('index.html', err=err)

def check_similarity(user_input):
    """
    Calculates the eucledian distance between the data points
    User to provide:
    - Quarter (NUM 1-5)
    - OFF_own_half (DUMMY 1: Offense on own half, 0: Offense on opponents half)
    - YardLine (NUM)
    - Distance to next first down(NUM)
    - Down (NUM 1-4)
    - DefendersInTheBox (NUM)
    """
    global data
    global sim

    X_data = sim.values
    # Scale similarity df and user data
    scaler2 = StandardScaler()
    X_data = scaler2.fit(X_data).transform(X_data.astype(float))
    user_input = scaler2.transform(user_input.astype(float))

    # calculate eucledian distance for each data point in the similarity df
    eucledian = []
    for i in X_data:
        dist = np.linalg.norm(i-user_input[0])
        eucledian.append(dist)
    
    # append eucledian distance to the data.csv
    data['eucledian'] = eucledian

    return data

@app.route('/predict', methods=['POST'])
def predict():
    # Get user_input from FlaskForm:
    Quarter = request.form.get('quarter')
    OFF_own_half = request.form.get('OFF_own_half')
    YardLine = request.form.get('YardLine')
    Distance = request.form.get('Distance')
    Down = request.form.get('Down')
    DefendersInTheBox = request.form['DefendersInTheBox']

    # Calculate Probability of First Down for each Offensive Formation
    OffenseFormations = {'SINGLEBACK': 0, 'SHOTGUN': 1, 'I_FORM': 2, 'PISTOL': 3, 'JUMBO': 4, 'WILDCAT': 5, 'EMPTY': 6}
    result = {'SINGLEBACK': 0, 'SHOTGUN': 0, 'I_FORM': 0, 'PISTOL': 0, 'JUMBO': 0, 'WILDCAT': 0, 'EMPTY': 0}
    global scaler
    global clm

    user_input = [Quarter, OFF_own_half, YardLine, Distance, Down, DefendersInTheBox] # user_input as list
    user_input = np.array(user_input)
    
    for offense in OffenseFormations:
        offense_vector = np.zeros(7) # create vector for ML Model
        offense_vector[OffenseFormations[offense]] = 1
        variables = np.concatenate((user_input, offense_vector))
        variables = variables.reshape(1,-1).astype(float)

        variables = scaler.transform(variables.astype(float))
        prediction = clm.predict(variables)
        prediction_proba = clm.predict_proba(variables)
        result[offense] = (prediction[0], round(prediction_proba[0][1], 4))
    
    # Check for similar play types based on the given user input and save it to eucledian_df
    user_input = user_input.reshape(1,-1).astype(float)
    eucledian_df = check_similarity(user_input)
    eucledian_df = eucledian_df.sort_values(by='eucledian', ascending=True)[:10]

    return render_template('prediction.html', result=result, eucledian_df=eucledian_df)

@app.route('/play/<PlayId>', methods=['GET'])
def show_play(PlayId):
    """
    Return Website which shows information on a specific play (input: PlayId)
    """
    # Retrieve general information from the train.csv (df)
    global df
    df_play = df.query(f"PlayId == {PlayId}")
    home_team = df_play['HomeTeamAbbr'].tolist()[0]
    visitor_team = df_play['VisitorTeamAbbr'].tolist()[0]
    yards_gained = df_play['Yards'].tolist()[0]
    game = visitor_team + ' at ' + home_team
    offense_team = df_play['OffensePersonnel'].tolist()[0]
    defendersinthebox = df_play['DefendersInTheBox'].tolist()[0]
    offense_formation = df_play['OffenseFormation'].tolist()[0]
    season = df_play['Season'].tolist()[0]
    week = df_play['Week'].tolist()[0]
    stadium = df_play['Stadium'].tolist()[0]


    result = {
        'home_team': home_team,
        'visitor_team': visitor_team,
        'yards_gained': yards_gained,
        'game': game,
        'offense_team': offense_team,
        'defendersinthebox': defendersinthebox,
        'offense_formation': offense_formation,
        'season': season,
        'week': week,
        'stadium': stadium
    }

    return render_template('play.html', result=result, PlayId=PlayId)

@app.route('/play.png/<PlayId>')
def play_png(PlayId):
    """
    Once show_play() function is called and the website calls the /play.png route, 
    this function returns the picture of the play with the individual 
    positions of each player on the field
    """
    global df
    fig, ax = plot_football_field()
    df_play = df.query(f"PlayId == {PlayId}")
    away = df_play.query("Team == 'away'")
    home = df_play.query("Team == 'home'")
    qb = df_play.query("Position == 'QB'")
    qb_name = qb['DisplayName'].tolist()[0]
    home_team = df_play['HomeTeamAbbr'].tolist()[0]
    home_score = df_play['HomeScoreBeforePlay'].tolist()[0]
    visitor_team = df_play['VisitorTeamAbbr'].tolist()[0]
    visitor_score = df_play['VisitorScoreBeforePlay'].tolist()[0]
    game = visitor_team + ' at ' + home_team
    score = visitor_team + ': ' + str(visitor_score) + '  |  ' + home_team + ': ' + str(home_score)
    down_dist = str(df_play['Down'].tolist()[0]) + ' and ' + str(df_play['Distance'].tolist()[0])
    game_clock = df_play['GameClock'].tolist()[0]
    quarter = str(df_play['Quarter'].tolist()[0])

    ax.scatter(away['X'], away['Y'], color='orange', s=35, label='Away: '+visitor_team)
    ax.scatter(home['X'], home['Y'], color='red', s=35, label='Home: '+home_team)
    ax.scatter(qb['X'], qb['Y'], color='black', s=45, label='QB: '+qb_name)
    ax.text(60, -4, game_clock+' (Q: '+quarter+')', horizontalalignment='center', fontsize=15,  color='black')
    ax.text(60, 54, score, horizontalalignment='center', fontsize=15,  color='black')
    ax.legend()
    ax.set_title(f'{game} - {down_dist}\nPlay #{PlayId}')

    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='image/png')

def plot_football_field(figsize=(12, 5.33)):

    # Create Field 120x53.3, starting point is (0, 0), zorder=0 to put it onto the bottom
    rect = patches.Rectangle((0, 0), 120, 53.3, linewidth=0.1, facecolor='darkgreen', zorder=0)
    fig = Figure(figsize=figsize)
    ax = fig.add_subplot(1, 1, 1)
    ax.add_patch(rect)
    
    # Plot white lines every 10 yards
    ax.plot([10, 10, 10, 20, 20, 30, 30, 40, 40, 50, 50, 60, 60, 70, 70, 80, 80, 90, 90, 100, 100, 110, 110, 120, 0, 0, 120, 120], 
    [0, 0, 53.3, 53.3, 0, 0, 53.3, 53.3, 0, 0, 53.3, 53.3, 0, 0, 53.3, 53.3, 0, 0, 53.3, 53.3, 0, 0, 53.3, 53.3, 53.3, 0, 0, 53.3],
    color='white')
    
    # plot the endzones (ez1, ez2)
    ez1 = patches.Rectangle((0, 0), 10, 53.3, linewidth=0.1, facecolor='red', alpha=0.5, zorder=1) # left ez
    ez2 = patches.Rectangle((110, 0), 120, 53.3, linewidth=0.1, facecolor='red', alpha=0.5, zorder=1) # right ez
    ax.add_patch(ez1)
    ax.add_patch(ez2)

    ax.set_xlim(0, 120)
    ax.set_ylim(-5, 58.3)
    ax.axis('off')

    # Numbers on playfield
    ax.text(30, 5, 20, horizontalalignment='center', fontsize=20,  color='white')
    ax.text(60, 5, 50, horizontalalignment='center', fontsize=20,  color='white')
    ax.text(90, 5, 20, horizontalalignment='center', fontsize=20,  color='white')
    ax.text(29.2, 53.3 - 5, 20, horizontalalignment='center', fontsize=20,  color='white', rotation=180)
    ax.text(59.2, 53.3 - 5, 50, horizontalalignment='center', fontsize=20,  color='white', rotation=180)
    ax.text(89.2, 53.3 - 5, 20, horizontalalignment='center', fontsize=20,  color='white', rotation=180)

    return fig, ax

@app.errorhandler(404)
def page_not_found(e):
    return redirect(url_for('index', err=True))

@app.errorhandler(500)
def internal_server_error(e):
    return redirect(url_for('index', err=True))

if __name__ == '__main__':

    # Load files into Memory
    ### train.csv: Full dataset from Kaggle with all information per player (https://www.kaggle.com/c/nfl-big-data-bowl-2020/data)
    filename = os.path.join(app.root_path, 'static', 'train.csv')
    df = pd.read_csv(filename)
    
    ### data.csv: Shortened train.csv dataset - data per play (not per player) and only relevant attributes
    filename = os.path.join(app.root_path, 'static', 'data.csv')
    data = pd.read_csv(filename)
    data.drop(['Unnamed: 0'], axis=1, inplace=True)

    ### similarity_df.csv: Shortened data.csv with only the attributes needed for the similarity check of play types
    filename = os.path.join(app.root_path, 'static', 'similarity_df.csv')
    sim = pd.read_csv(filename)
    sim.drop(['Unnamed: 0'], axis=1, inplace=True)

    ### Load scaler & trained ML Model
    filename = os.path.join(app.root_path, 'static', 'scaler.sav')
    scaler = pkl.load(open(filename, 'rb'))
    filename = os.path.join(app.root_path, 'static', 'model.p')
    clm = pkl.load(open(filename, 'rb'))

    # Run app
    if len(sys.argv) > 1:
        if sys.argv[1] == '--debug':
            app.run(host='0.0.0.0', debug=True, port=5000)
    else:
        app.run(host='0.0.0.0', debug=False)